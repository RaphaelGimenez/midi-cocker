import type { NextPage } from 'next'
import Layout from '../components/Layout'

const Home: NextPage = () => {
  return <Layout>content</Layout>
}

export default Home
