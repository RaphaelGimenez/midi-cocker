import {
  Box,
  Button,
  Divider,
  NativeSelect,
  NumberInput,
  SimpleGrid,
  Switch,
  Title,
} from '@mantine/core'
import { IconPlus } from '@tabler/icons'
import { NextPage } from 'next'
import MidiOutDeviceForm from '../components/devices/midi-out-device-form'
import Layout from '../components/Layout'

const Devices: NextPage = () => {
  return (
    <Layout>
      <Title order={1}>MIDI Devices</Title>

      <Title order={2} mt="xl" mb="sm">
        MIDI In device
      </Title>
      <NativeSelect
        data={['Arturia BSP IN', 'Volca Sample IN', 'Launchpad S', 'Win In']}
        placeholder="Select MIDI In device"
        label="MIDI In"
        labelProps={{ sx: { fontWeight: 'bold' } }}
        description="Select your MIDI In device"
        withAsterisk
      />

      <Title order={2} mt="xl" mb="sm">
        MIDI Out devices
      </Title>
      <MidiOutDeviceForm />

      <Button leftIcon={<IconPlus />} color="dark" size="sm">
        Add MIDI Out device
      </Button>
    </Layout>
  )
}

export default Devices
