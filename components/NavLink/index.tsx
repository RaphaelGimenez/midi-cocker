import { UnstyledButton, Group, ThemeIcon, Text } from '@mantine/core'
import Link from 'next/link'

interface NavLinkProps {
  icon: React.ReactNode
  color: string
  label: string
  href: string
}

const NavLink = ({ icon, color, label, href }: NavLinkProps) => {
  return (
    <Link href={href} passHref>
      <UnstyledButton
        component="a"
        href={href}
        sx={(theme) => ({
          display: 'block',
          width: '100%',
          padding: theme.spacing.xs,
          borderRadius: theme.radius.sm,
          color:
            theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.black,

          '&:hover': {
            backgroundColor:
              theme.colorScheme === 'dark'
                ? theme.colors.dark[6]
                : theme.colors.gray[0],
          },
        })}
      >
        <Group>
          <ThemeIcon color={color} variant="light">
            {icon}
          </ThemeIcon>

          <Text size="sm">{label}</Text>
        </Group>
      </UnstyledButton>
    </Link>
  )
}

export default NavLink
