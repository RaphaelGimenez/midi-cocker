import { AppShell, Navbar, Header, Text } from '@mantine/core'
import { IconHome, IconWaveSine } from '@tabler/icons'
import NavLink from '../NavLink'

const data = [
  {
    icon: <IconHome size={16} />,
    color: 'blue',
    label: 'Home page',
    href: '/',
  },
  {
    icon: <IconWaveSine size={16} />,
    color: 'green',
    label: 'MIDI Devices',
    href: '/devices',
  },
]

interface LayoutProps {
  children: React.ReactNode
}

const Layout = ({ children }: LayoutProps) => (
  <AppShell
    padding="md"
    navbar={
      <Navbar width={{ base: 300 }} p="xs">
        <Navbar.Section grow mt="xs">
          {data.map((link) => (
            <NavLink {...link} key={link.label} />
          ))}
        </Navbar.Section>
      </Navbar>
    }
    header={
      <Header height={60} p="xs" sx={{ display: 'flex', alignItems: 'center' }}>
        <Text size="xl" weight="bold" component="span">
          MIDICocker
        </Text>
      </Header>
    }
    styles={(theme) => ({
      main: {
        backgroundColor:
          theme.colorScheme === 'dark'
            ? theme.colors.dark[8]
            : theme.colors.gray[0],
      },
    })}
  >
    {children}
  </AppShell>
)

export default Layout
