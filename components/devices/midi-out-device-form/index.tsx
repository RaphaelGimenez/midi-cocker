import {
  SimpleGrid,
  NativeSelect,
  NumberInput,
  Switch,
  Button,
  Divider,
} from '@mantine/core'
import { IconPlus } from '@tabler/icons'

const MidiOutDeviceForm = () => {
  return (
    <>
      <SimpleGrid cols={2}>
        <NativeSelect
          data={[
            'Arturia BSP OUT',
            'Volca Sample OUT',
            'Launchpad S',
            'Win Out',
          ]}
          placeholder="Select MIDI Out device"
          label="MIDI Out"
          labelProps={{ sx: { fontWeight: 'bold' } }}
          description="Select your MIDI Out device"
          withAsterisk
        />
        <SimpleGrid cols={1} verticalSpacing="xs">
          <NumberInput
            defaultValue={1}
            min={1}
            max={16}
            placeholder="1"
            label="Assigned MIDI Channel(s)"
            description="Enter MIDI channel assigned to this device"
            withAsterisk
          />
          <Switch label="Range" size="md" radius="sm" />
          <Button
            leftIcon={<IconPlus />}
            color="dark"
            size="xs"
            sx={{ width: 'fit-content' }}
          >
            Add MIDI Channel
          </Button>
        </SimpleGrid>
      </SimpleGrid>
      <Divider my="md" />
    </>
  )
}

export default MidiOutDeviceForm
