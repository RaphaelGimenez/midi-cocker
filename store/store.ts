import create, { StateCreator } from 'zustand'
import produce from 'immer'

interface DevicesSlice {
  MIDIDeviceIn?: {
    id: string
    name: string
  }
  MIDIDevicesOut: {
    id: string
    name: string
    MIDIChannels: number[]
  }[]
  setMIDIDeviceIn: (id: string, name: string) => void
  setMIDIDeviceOut: (id: string, name: string) => void
}
const createDevicesSlice: StateCreator<
  DevicesSlice & MIDISlice,
  [],
  [],
  DevicesSlice
> = (set) => ({
  MIDIDeviceIn: undefined,
  MIDIDevicesOut: [],
  setMIDIDeviceIn: (id, name) => set({ MIDIDeviceIn: { id, name } }),
  setMIDIDeviceOut: (id, name) =>
    set((state) =>
      produce(state, (draft) => {
        draft.MIDIDevicesOut.push({ id, name, MIDIChannels: [] })
      })
    ),
})

interface MIDISlice {
  MIDIAccess?: WebMidi.MIDIAccess
  MIDIInputs?: WebMidi.MIDIInputMap
  MIDIOutputs?: WebMidi.MIDIOutputMap
  connectMIDI: (access: WebMidi.MIDIAccess) => void
}
const createMIDISlice: StateCreator<
  MIDISlice & DevicesSlice,
  [],
  [],
  MIDISlice
> = (set) => ({
  connectMIDI: (access) =>
    set(() => ({
      MIDIAccess: access,
      MIDIInputs: access.inputs,
      MIDIOutputs: access.outputs,
    })),
})

const useStore = create<DevicesSlice & MIDISlice>()((...a) => ({
  ...createDevicesSlice(...a),
  ...createMIDISlice(...a),
}))

export default useStore
