describe('App layout', () => {
  it('should display layout', () => {
    cy.visit('/')
    cy.findByRole('banner').should('exist')
    cy.findByRole('navigation')
      .should('exist')
      .within(() => {
        cy.findByRole('link', { name: /home page/i }).should('exist')
        cy.findByRole('link', { name: /midi devices/i }).should('exist')
      })
  })
})
